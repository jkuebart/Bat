# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
HitBtc represents a connection to the hitbtc endpoint and provides methods
for accessing its public API methods.
"""

__all__ = ["HitBtc"]

try: # Python 3.5+
        from http import HTTPStatus
except ImportError:
        from http import client as HTTPStatus
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from bat import json
from bat.decimals import decimals
from bat.ticker import Ticker

VERSION = "2"
URL_PATH = "/%s/%s"

def make_url(action, realm=None):
        """Return the URL path for the given action and realm."""
        if realm:
                action = "%s/%s" % (realm, action)
        return URL_PATH % (VERSION, action)

def get_result(response):
        """Return the result of the response, raise on error."""
        resp = json.load(response)
        if HTTPStatus.OK != response.status:
                raise RuntimeError(
                        "%s: %s (%s)" % (resp["error"]["code"],
                                         resp["error"]["message"],
                                         resp["error"]["description"])
                )
        return resp

class HitBtc:
        """Interact with HitBtc's API."""

        def __init__(self, api_host):
                self.api_host = api_host

        def url(self, url_path):
                """Build an API URL from the given URL path."""
                return "%s%s" % (self.api_host, url_path)

        def load(self, action, realm=None, data=None, headers={}, method=None):
                """Return the result of the specified query."""
                url = self.url(make_url(action, realm))
                data = data and urlencode(data).encode("ascii")
                with urlopen(Request(url, data, headers, method=method)) as resp:
                        return get_result(resp)

        def request(self, action, data=None, headers={}, method=None):
                """Send a public request to the API."""
                return self.load(action, "public", data, headers, method)

        def asset_pairs(self):
                """Invoke the symbol API method."""
                return {
                        pair["id"]: {
                                "base": pair["baseCurrency"],
                                "price_decimals": decimals(float(pair["tickSize"])),
                                "quote": pair["quoteCurrency"],
                                "volume_decimals": decimals(float(pair["quantityIncrement"])),
                        }
                        for pair in self.request("symbol")
                }

        def ticker(self, asset_pairs):
                """Invoke the ticker API method."""
                return Ticker(
                        (data["symbol"],
                         asset_pairs[data["symbol"]]["base"],
                         asset_pairs[data["symbol"]]["quote"],
                         float(data["ask"]),
                         float(data["bid"])
                        )
                        for data in self.request("ticker")
                        if (data["symbol"] in asset_pairs and
                            data["ask"] is not None and
                            data["bid"] is not None)
                )
