# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Account represents a connection to some bittrex account and provides
methods for accessing its private API methods.
"""

__all__ = ["Account"]

from hashlib import sha512
import hmac

from bat.noncegenerator import NonceGenerator
from bat.trade import Trade

from .bittrex import Bittrex, load

class Account:
        """Interact with Bittrex's private API."""

        def __init__(self, config):
                self._bittrex = Bittrex(config["apiHost"])
                self._public_key = config["publicKey"]
                self._secret_key = config["secretKey"].encode("utf-8")

                self._nonce = NonceGenerator()

        def xchg(self):
                return self._bittrex

        def request(self, method, realm, data={}):
                """Send a signed request to this account."""
                data = dict(data)
                data["apikey"] = self._public_key
                data["nonce"] = self._nonce.get_nonce()
                url = self._bittrex.url(method, realm, data)

                sign = hmac.new(self._secret_key, url.encode("utf-8"), sha512)
                return load(url, {"apisign": sign.hexdigest()})

        def balance(self):
                """Invoke the getbalances API method."""
                return {
                        bal["Currency"]: bal["Balance"]
                        for bal in self.request("getbalances", "account")
                        if bal["Balance"]
                }

        def open_orders(self):
                """Invoke the getopenorders API method."""
                return {
                        ordr["OrderUuid"]: Trade(
                                "buy" if "LIMIT_BUY" == ordr["OrderType"] else "sell",
                                ordr["Exchange"],
                                ordr["QuantityRemaining"],
                                ordr["Limit"]
                        )
                        for ordr in self.request("getopenorders", "market")
                }

        def cancel_order(self, txid):
                """Invoke the cancel API method."""
                self.request("cancel", "market", {"uuid": txid})
                return self

        def add_trade(self, trade, dry_run=False):
                """Invoke the buylimit or selllimit API method."""
                order = {
                        "market": trade.pair,
                        "rate": trade.format_price(),
                        "quantity": trade.format_volume(),
                }
                if dry_run:
                        return order
                method = "buylimit" if "buy" == trade.type else "selllimit"
                return self.request(method, "market", order)
