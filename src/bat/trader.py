# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
A Trader produces Trades from a Normalisation and asset-agnostic normalised
amounts.
"""

__all__ = ["Trader"]

from .trade import Trade

class Trader:
        """
        I can turn tradeable asset pairs and normalised amounts into actual
        trades.
        """

        def __init__(self, normalisation):
                """Create a new Trader with the given normalisation."""
                self.normalisation = normalisation

        def make_trade(self, amount, pair_name, asset_pair):
                """
                Return a Trade representing the normalised amount for the
                asset_pair. A positive amount leads to buying the pair's
                "base" currency at its asking price, while a negative
                amount leads to selling it at the bidding price.

                    >>> from unittest.mock import Mock

                    >>> normalisation = Mock()
                    >>> normalisation.valuate.return_value = 4
                    >>> ticker = normalisation.get_ticker.return_value
                    >>> ticker.ask.return_value = 2
                    >>> ticker.bid.return_value = 3

                    >>> t = Trader(normalisation)
                    >>> asset_pair = {"base": "a0", "quote": "a1"}

                    >>> t.make_trade(1, "pair", asset_pair)
                    ... # doctest: +ELLIPSIS
                    Trade('buy', 'pair', 0.25, 2, {...})

                    >>> t.make_trade(-2, "pair", asset_pair)
                    ... # doctest: +ELLIPSIS
                    Trade('sell', 'pair', 0.5, 3, {...})
                """
                ticker = self.normalisation.get_ticker()

                base, quote = asset_pair["base"], asset_pair["quote"]
                if 0 < amount:
                        trade_type = "buy"
                        price = ticker.ask(base, quote)
                else:
                        trade_type = "sell"
                        price = ticker.bid(base, quote)

                volume = abs(amount) / self.normalisation.valuate(base)

                return Trade(trade_type, pair_name, volume, price, asset_pair)

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
