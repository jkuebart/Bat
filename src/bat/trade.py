# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Trade represents a trade of a given type, currency pair, and volume for a
given price.
"""

__all__ = ["Trade"]

from bat.isclose import isclose

def format_decimals(value, precision):
        """
        Format a value as a string with the given precision.

        If 0 <= precision, the value is rounded to a power of ten.

            >>> format_decimals(345.67, 0)
            '346'
            >>> format_decimals(345.67, 1)
            '350'

        If precision < 0, the value is formatted with the given number of
        digits.

            >>> format_decimals(345.67, -1)
            '345.7'
        """
        fmt = "{:.%df}" % (max(0, -precision),)
        return fmt.format(round(value, -precision))

class Trade:
        """
        I represent a trade of a given type, currency pair, and volume for
        a given price.
        """

        def __init__(self, trade_type, pair, volume, price, asset_pair=None):
                """
                Create a new trade.

                    trade_type - "buy" or "sell"
                    pair - name of the currency pair
                    volume - the amount
                    price - the price
                    asset_pair - an optional asset pair dict

                If specified, the keys "volume_decimals" and
                "price_decimals" must provide integral values which are
                used to format the trade's volume and price, respecively.
                """
                self.type = trade_type
                self.pair = pair
                self.volume = volume
                self.price = price
                self.asset_pair = asset_pair

        def __repr__(self):
                """
                Produce a string representation of the
                trade.

                    >>> repr(Trade("type", "pair", 123, 987, {}))
                    "Trade('type', 'pair', 123, 987, {})"
                """
                return "%s(%r, %r, %r, %r, %r)" % (
                        self.__class__.__name__,
                        self.type,
                        self.pair,
                        self.volume,
                        self.price,
                        self.asset_pair,
                )

        def __str__(self):
                """
                Produce a textual description of the
                trade.

                    >>> str(Trade("buy", "abc-xyz", 123, 987))
                    'buy 123 abc-xyz for 987'
                """
                return "%s %s %s for %s" % (
                        self.type,
                        self.format_volume(),
                        self.pair,
                        self.format_price(),
                )

        def matches(self, other, error):
                """
                Determine whether trades match (within a margin of error)
                for the purpose of re-use.

                    >>> t0 = Trade("", "", 123, 987)
                    >>> t0.matches(t0, 0)
                    True
                    >>> t1 = Trade("", "", 124, 986)
                    >>> t0.matches(t1, 0.001)
                    False
                    >>> t0.matches(t1, 0.01)
                    True
                """
                return (self.pair == other.pair and
                        self.type == other.type and
                        isclose(self.volume, other.volume, rel_tol=error) and
                        isclose(self.price, other.price, rel_tol=error))

        def format_price(self):
                """
                Format the price of this trade according to the asset
                pair's precision.

                    >>> t = Trade("", "", 0, 345.67, {"price_decimals": -1})
                    >>> t.format_price()
                    '345.7'
                    >>> t = Trade("", "", 0, 345.67, {"price_decimals": 1})
                    >>> t.format_price()
                    '350'
                """
                if self.asset_pair is None:
                        return self.price
                return format_decimals(self.price,
                                       self.asset_pair["price_decimals"])

        def eff_volume(self):
                """
                Return the price rounded to the asset pair's
                precision.

                    >>> t = Trade("", "", 345.67, 0, {"volume_decimals": -1})
                    >>> t.eff_volume()
                    345.7
                    >>> t = Trade("", "", 345.67, 0, {"volume_decimals": 1})
                    >>> t.eff_volume()
                    350.0
                """
                if self.asset_pair is None:
                        return self.volume
                return round(self.volume, -self.asset_pair["volume_decimals"])

        def format_volume(self):
                """
                Format the volume of this trade according to the asset
                pair's precision.

                    >>> t = Trade("", "", 345.67, 0, {"volume_decimals": -1})
                    >>> t.format_volume()
                    '345.7'
                    >>> t = Trade("", "", 345.67, 0, {"volume_decimals": 1})
                    >>> t.format_volume()
                    '350'
                """
                if self.asset_pair is None:
                        return self.volume
                return format_decimals(self.volume,
                                       self.asset_pair["volume_decimals"])

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
