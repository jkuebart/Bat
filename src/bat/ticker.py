# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""Ticker builds a table of Conversions for a list of assets."""

__all__ = ["Ticker"]

import logging

from .conversion import Conversion

LOGGER = logging.getLogger(__name__)

class Ticker:
        """
        I build a table of Conversions for a list of assets.

        The ticker contains Conversions for trading each asset_pair in both
        directions:

            >>> from itertools import product

            >>> asset_pairs = [("A-B", "A", "B", 4, 2)]
            >>> t = Ticker(asset_pairs)
            >>> list((base, quote, t.conversion(base, quote))
            ...     for base, quote in
            ...         product(sorted(t.get_assets()), repeat=2))
            ... # doctest: +NORMALIZE_WHITESPACE
            [('A', 'A', Conversion(1,   1,    [])),
             ('A', 'B', Conversion(4,   2,    ['A-B'])),
             ('B', 'A', Conversion(0.5, 0.25, ['A-B'])),
             ('B', 'B', Conversion(1,   1,    []))]

        Conversions are automatically added for pairs that are not
        directly tradeable:

            >>> asset_pairs += [("B-C", "B", "C", 16, 8)]
            >>> t = Ticker(asset_pairs)
            >>> list((base, quote, t.conversion(base, quote))
            ...     for base, quote in
            ...         product(sorted(t.get_assets()), repeat=2))
            ... # doctest: +NORMALIZE_WHITESPACE
            [('A', 'A', Conversion(1,      1,        [])),
             ('A', 'B', Conversion(4,      2,        ['A-B'])),
             ('A', 'C', Conversion(64,     16,       ['A-B', 'B-C'])),
             ('B', 'A', Conversion(0.5,    0.25,     ['A-B'])),
             ('B', 'B', Conversion(1,      1,        [])),
             ('B', 'C', Conversion(16,     8,        ['B-C'])),
             ('C', 'A', Conversion(0.0625, 0.015625, ['B-C', 'A-B'])),
             ('C', 'B', Conversion(0.125,  0.0625,   ['B-C'])),
             ('C', 'C', Conversion(1,      1,        []))]

        When there is more than one way to trade two assets, the most
        favourable route is chosen according to Conversion's definition of
        »favourable«.

            >>> asset_pairs += [
            ...     ("A-D", "A", "D", 9, 8),
            ...     ("C-D", "C", "D", 0.5, 0.125),
            ... ]
            >>> t = Ticker(asset_pairs)
            >>> t.conversion("B", "A") + t.conversion("A", "D")
            Conversion(4.5, 2.0, ['A-B', 'A-D'])
            >>> t.conversion("B", "C") + t.conversion("C", "D")
            Conversion(8.0, 1.0, ['B-C', 'C-D'])
            >>> t.conversion("B", "D")
            Conversion(4.5, 2.0, ['A-B', 'A-D'])
        """

        def __init__(self, asset_pairs):
                """
                Create a new Ticker with the given asset pairs.

                The asset_pairs should be tuples of the form

                    (pair_name, base, quote, ask, bid)
                """

                # Convert to internal format
                self._ticker = {}
                for pair_name, base, quote, ask, bid in asset_pairs:
                        self._add_conversion(
                                base, quote,
                                Conversion(ask, bid, [pair_name])
                        )
                        self._add_conversion(
                                quote, base,
                                Conversion(1 / bid, 1 / ask, [pair_name])
                        )

                # Complete the graph
                while self._fill_table():
                        pass

                LOGGER.debug("ticker %r", self._ticker)

        def _add_conversion(self, base, quote, conversion):
                if base not in self._ticker:
                        self._ticker[base] = {base: Conversion(1, 1, [])}
                self._ticker[base][quote] = conversion
                return self

        def _fill_table(self):
                modified = False
                for tick in self._ticker.values():
                        for base0, tick0 in self._ticker.items():
                                if tick is tick0 or base0 not in tick:
                                        continue
                                conv = tick[base0]
                                for quote0, conv0 in tick0.items():
                                        # ? -> conv -> base0
                                        # base0 -> conv0 -> quote0
                                        # ? -> new_conv -> quote0
                                        new_conv = conv + conv0
                                        if quote0 not in tick or new_conv < tick[quote0]:
                                                modified = True
                                                tick[quote0] = new_conv
                                                LOGGER.debug("added ? -> %r -> %r %r", base0, quote0, new_conv)

                return modified

        def __contains__(self, asset):
                """
                Determine whether the ticker knows about a certain asset.
                """
                return asset in self._ticker

        def get_assets(self):
                """
                Return a list of all asset names in this
                Ticker.

                    >>> t = Ticker([
                    ...     ("A-B", "A", "B", 1, 1),
                    ...     ("B-C", "B", "C", 1, 1),
                    ... ])
                    >>> sorted(t.get_assets())
                    ['A', 'B', 'C']
                """
                return self._ticker.keys()

        def conversion(self, base, quote):
                """
                Return the conversion for a pair of assets that may not be
                a tradeable pair.

                    >>> t = Ticker([
                    ...     ("A-B", "A", "B", 2, 1),
                    ...     ("B-C", "B", "C", 4, 2),
                    ... ])
                    >>> t.conversion("A", "C")
                    Conversion(8, 2, ['A-B', 'B-C'])
                    >>> t.conversion("C", "A")
                    Conversion(0.5, 0.125, ['B-C', 'A-B'])
                """
                return self._ticker[base][quote]

        def ask(self, base, quote):
                """
                Return the ask price for base in terms of
                quote.

                    >>> t = Ticker([
                    ...     ("A-B", "A", "B", 2, 1),
                    ...     ("B-C", "B", "C", 4, 2),
                    ... ])
                    >>> t.ask("A", "C")
                    8
                """
                return self.conversion(base, quote).ask

        def bid(self, base, quote):
                """
                Return the bid price for base in terms of
                quote.

                    >>> t = Ticker([
                    ...     ("A-B", "A", "B", 2, 1),
                    ...     ("B-C", "B", "C", 4, 2),
                    ... ])
                    >>> t.bid("A", "C")
                    2
                """
                return self.conversion(base, quote).bid

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
