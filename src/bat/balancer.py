# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
A Balancer accumulates trades needed to achieve given differences.
"""

__all__ = ["Balancer"]

import logging

LOGGER = logging.getLogger(__name__)

class Balancer:
        """I accumulate trades required to achieve given trade differences."""

        def __init__(self, asset_pairs, ticker):
                """Create a new Balancer with asset_pairs and a ticker."""
                self.asset_pairs = asset_pairs
                self.ticker = ticker

                self._balances = {}

        def trade(self, amount, base, quote):
                """
                Adjust balances to reflect trading the given amount of base
                for quote.

                    >>> from unittest.mock import Mock

                    >>> asset_pairs = {
                    ...     "A-B": {"base": "A", "quote": "B"},
                    ...     "B-C": {"base": "B", "quote": "C"},
                    ...     "C-D": {"base": "C", "quote": "D"},
                    ... }
                    >>> conversions = {
                    ...     "A": {"D": ["A-B", "B-C", "C-D"]},
                    ...     "D": {"A": ["C-D", "B-C", "A-B"]},
                    ... }

                    >>> ticker = Mock()
                    >>> ticker.conversion = lambda b, q: conversions[b][q]

                    >>> b = Balancer(asset_pairs, ticker)
                    >>> b.trade(3, "A", "D")
                    >>> sorted(b._balances.items())
                    [('A-B', 3), ('B-C', 3), ('C-D', 3)]

                    >>> b = Balancer(asset_pairs, ticker)
                    >>> b.trade(3, "D", "A")
                    >>> sorted(b._balances.items())
                    [('A-B', -3), ('B-C', -3), ('C-D', -3)]
                """
                pair_names = self.ticker.conversion(base, quote)
                for pair_name in pair_names:
                        asset_pair = self.asset_pairs[pair_name]

                        if pair_name not in self._balances:
                                self._balances[pair_name] = 0

                        if base == asset_pair["base"]:
                                self._balances[pair_name] += amount
                                base = asset_pair["quote"]
                        else:
                                self._balances[pair_name] -= amount
                                base = asset_pair["base"]

                        LOGGER.debug("balances %r", self._balances)

        def make_trades(self, trader):
                """
                Return trades created by the trader for the
                balances.

                    >>> from unittest.mock import Mock

                    >>> conversions = {"A": {"B": ["A-B"], "C": ["A-B", "B-C"]}}
                    >>> ticker = Mock()
                    >>> ticker.conversion = lambda b, q: conversions[b][q]

                    >>> asset_pairs = {
                    ...     "A-B": {"base": "A", "quote": "B"},
                    ...     "B-C": {"base": "B", "quote": "C"},
                    ... }

                    >>> b = Balancer(asset_pairs, ticker)

                    >>> balances = {}
                    >>> trader = Mock()
                    >>> trader.make_trade = lambda a, p, _: balances.__setitem__(p, a)

                    >>> b.trade(1, "A", "B")
                    >>> b.trade(1, "A", "C")
                    >>> for _ in b.make_trades(trader): pass
                    >>> sorted(balances.items())
                    [('A-B', 2), ('B-C', 1)]
                """
                for pair_name, amount in self._balances.items():
                        asset_pair = self.asset_pairs[pair_name]
                        yield trader.make_trade(amount, pair_name, asset_pair)

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
