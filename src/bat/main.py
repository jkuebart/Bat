# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Balanced Auto Trade

Keep a number of assets in balance with respect to various valuations.
"""

__all__ = ["main"]

from bisect import bisect
from importlib import import_module
import logging

from bat.balancer import Balancer
from bat.normalisation import Normalisation
from bat.trader import Trader

LOGGER = logging.getLogger(__name__)

def create_account(config):
        """Create an instance of the account class specified by the
        configuration."""
        acc_mod_name, acc_cls_name = config["class"].rsplit(".", 1)
        acc_mod = import_module(acc_mod_name)
        return getattr(acc_mod, acc_cls_name)(config)

def get_diff(balance, portfolio):
        """
        Compute an ordered list of (difference, asset) pairs between the
        balances and the portfolio.

            >>> get_diff(
            ...     { "A": 3, "C": 3, "D": 1 },
            ...     { "B": 2, "C": 2, "D": 3 }
            ... )
            [(-3.0, 'A'), (-1.0, 'C'), (2.0, 'B'), (2.0, 'D')]
        """

        # Calculate target portfolio
        portfolio_factor = sum(balance.values()) / sum(portfolio.values())
        want_norm = {
                base: val * portfolio_factor
                for base, val in portfolio.items()
        }
        LOGGER.info("want_norm %r", want_norm)

        # Calculate portfolio differences
        return sorted(
                (want_norm.get(base, 0.0) - balance.get(base, 0), base)
                for base in want_norm.keys() | balance.keys()
        )

def get_balances(want_diff, balancer, threshold):
        """
        Add balances for the differences to the balancer. Differences are
        ignored if the incoming amount is below the threshold *and* the
        outgoing amount is below the double threshold. This asymmetry is
        intended to bias trading towards realising higher gains.

            >>> from unittest.mock import Mock

            >>> balances = {}

            >>> balancer = Mock()
            >>> balancer.trade = lambda a, b, q: balances.__setitem__(b + q, a)

            >>> diffs = [(-3, "A"), (-1, "C"), (2, "B"), (2, "D")]
            >>> get_balances(diffs, balancer, 2)
            >>> sorted(balances.items())
            [('BA', 1), ('BC', 1), ('DA', 2)]
            >>> balances.clear()

            >>> diffs = [(-3, "A"), (-1, "C"), (1, "B"), (1, "D")]
            >>> get_balances(diffs, balancer, 2)
            >>> sorted(balances.items())
            []
        """
        LOGGER.info("want_diff %r", want_diff)
        skip_from = bisect(want_diff, (-2 * threshold, ""))
        skip_to = bisect(want_diff, (threshold, ""))
        LOGGER.info("skipping %r %r", skip_from, skip_to)
        while ((0 < skip_from or skip_to < len(want_diff)) and
               want_diff[0][0] < 0 and 0 < want_diff[-1][0]):
                # Process one order.
                diff_out, diff_in = want_diff[0], want_diff[-1]
                amount_out, amount_in = abs(diff_out[0]), abs(diff_in[0])
                quote, base = diff_out[1], diff_in[1]

                # Remove the smaller amount from list of differences.
                want_diff = want_diff[1:-1]
                if amount_out < amount_in:
                        want_diff.append((amount_in - amount_out, base))
                elif amount_in < amount_out:
                        want_diff.insert(0, (amount_in - amount_out, quote))

                # Compute the amount for this trade.
                if amount_out <= amount_in:
                        amount = amount_out
                        skip_from -= 1
                        skip_to -= 1
                else:
                        amount = amount_in

                # Add this difference to the balancer.
                balancer.trade(amount, base, quote)
                LOGGER.debug("want_diff %r", want_diff)

def tabular(rows):
        """
        Nicely format the data rows as a
        table.

            >>> tabular([])
            ''
            >>> tabular([["1", "12", "123"]])
            ' 1 | 12 | 123 '
            >>> tabular([["", ".", "123"], ["1", "12", "."]])
            '   |  . | 123 \\n 1 | 12 |   . '
        """
        fmt = "|".join(
                " {:>%d} " % (max(len(cell) for cell in col),)
                for col in zip(*rows)
        )

        return "\n".join(fmt.format(*row) for row in rows)

def trades_for_portfolio(xchg, balance, portfolio, error):
        """
        Compute required trades to match current balance to desired
        portfolio.
        """

        # Select relevant asset pairs.
        assets = frozenset(balance.keys() | portfolio.keys())
        asset_pairs = {
                pair_name: asset_pair
                for pair_name, asset_pair in xchg.asset_pairs().items()
                if (asset_pair["base"] in assets and
                    asset_pair["quote"] in assets)
        }
        LOGGER.debug("asset_pairs %r", asset_pairs)

        # Get ticker prices for tradeable pairs and calculate assets"
        # normalisation factors.
        ticker = xchg.ticker(asset_pairs)
        normalisation = Normalisation(ticker)

        # Bugfix: delete assets that are missing from the Ticker,
        # like DOGE on HitBTC.
        for asset in list(balance.keys()):
                if asset not in ticker:
                        LOGGER.error("%r: balance asset not in ticker", asset)
                        del balance[asset]
        for asset in list(portfolio.keys()):
                if asset not in ticker:
                        LOGGER.error("%r: portfolio asset not in ticker", asset)
                        del portfolio[asset]

        # Calculate normalised valuations
        balance_norm = {
                base: val * normalisation.valuate(base)
                for base, val in balance.items()
        }
        LOGGER.info("balance_norm %r", balance_norm)
        holdings = {
                base: sum(balance_norm.values()) / normalisation.valuate(base)
                for base in balance_norm
        }
        LOGGER.info("holdings %r", holdings)

        # Convert portfolio differences into balances of tradeable pairs
        want_diff = get_diff(balance_norm, portfolio)

        # Show data
        print(tabular(
                [["asset", "balance", "holdings", "diff"]] +
                [
                        [
                                asset,
                                "{:.2f}".format(balance.get(asset, 0)),
                                "{:.2f}".format(holdings.get(asset, 0)),
                                "{:.2f}".format(diff),
                        ] for diff, asset in want_diff
                ]
        ))

        # Compute asset pair balances from differences.
        balancer = Balancer(asset_pairs, ticker)
        threshold = error * sum(balance_norm.values())
        LOGGER.info("threshold %r", threshold)
        get_balances(want_diff, balancer, threshold)

        # Create trades from the balances for different assets.
        return filter(
                lambda t: 0 != t.eff_volume(),
                balancer.make_trades(Trader(normalisation))
        )

def find_trades(trades, open_orders, error):
        """From a list of desired trades and currently open orders, produce
        lists of orders to be added and cancelled, respectively."""

        # Check for existing, conflicting orders
        new_orders = []
        cancel_orders = set()
        for trade in trades:
                orderid = None
                new_open_orders = {}
                for txid, ordr in open_orders.items():
                        if ordr.pair != trade.pair:
                                new_open_orders[txid] = ordr
                        elif orderid is None and trade.matches(ordr, error):
                                print("Reusing %r %r" % (txid, ordr))
                                orderid = txid
                        else:
                                LOGGER.info("Cancelling %r %r", txid, ordr)
                                cancel_orders.add(txid)
                open_orders = new_open_orders

                if orderid is None:
                        new_orders.append(trade)

        return new_orders, cancel_orders

def main(config, dry_run=False):
        # Load user's balance.
        account = create_account(config["account"])
        balance = account.balance()
        LOGGER.info("balance %r", balance)

        # Convert portfolio differences into trades.
        portfolio = config["portfolio"]
        trades = list(trades_for_portfolio(
                account.xchg(), balance, portfolio, config["error"]
        ))
        LOGGER.debug("trades %r", trades)

        # Cancel and add orders.
        open_orders = account.open_orders()
        for order in open_orders.values():
                print("Open order %s." % order)
        new_orders, cancel_orders = find_trades(
                trades, open_orders, config["error"]
        )

        for txid in cancel_orders:
                print(
                        "Cancelling order to %s%s." %
                        (open_orders[txid], " (dry run)" if dry_run else "")
                )
                if not dry_run:
                        account.cancel_order(txid)

        for trade in new_orders:
                print(
                        "Ordering to %s%s." %
                        (trade, " (dry run)" if dry_run else "")
                )
                try:
                        res = account.add_trade(trade, dry_run)
                        LOGGER.info("add_trade %r", res)
                except RuntimeError as exc:
                        print("add_trade %s" % (exc,))

        print("")

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
