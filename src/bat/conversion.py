# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Conversion supports conversions between asset pairs that may not be
directly tradeable.
"""

__all__ = ["Conversion"]

class Conversion:
        """
        I represent a conversion between an asset pair that may not be
        directly tradeable. I hold a sequence of intermediate trades that
        ends in the quote asset.

        Conversions are ordered by their simplicity (length of the trading
        sequence) and favourability (lowest ask and highest bid prices).

        Conversions can be concatenated in the obvious way.
        """

        def __init__(self, ask, bid, seq):
                """
                Create a new conversion.

                    ask - The asking price
                    bid - The bidding price
                    seq - A sequence of assets ending in the quote asset

                Ask and bid prices are understood in terms of the quote
                asset.
                """
                self.ask = ask
                self.bid = bid
                self.seq = seq

        def __repr__(self):
                """
                Provide a string representation of this
                conversion.

                    >>> repr(Conversion(0, 1, ["A", "B"]))
                    "Conversion(0, 1, ['A', 'B'])"
                """
                return "%s(%r, %r, %r)" % (self.__class__.__name__,
                                           self.ask,
                                           self.bid,
                                           self.seq)

        def __lt__(self, other):
                """
                An ordering of conversions. Shorter conversions are always
                preferable as they require fewer trades, each of which
                could potentially fail to complete.

                    >>> c0 = Conversion(0, 0, ["B"])
                    >>> c1 = Conversion(0, 0, ["A", "B"])
                    >>> c0 < c1
                    True

                Note that comparisons are only meaningful between
                conversions with the same quote asset, but this is not
                enforced.

                Given sequences of equal length, a conversion is preferable
                if it has

                  * a lower ask price and simultaneously no lower bid
                    price or
                  * no higher ask price and simultaneously a higher bid
                    price.

                Example:

                    >>> c0 = Conversion(2, 3, ["A", "B"])
                    >>> c1 = Conversion(1, 3, ["A", "B"])
                    >>> c1 < c0
                    True
                    >>> c2 = Conversion(2, 4, ["A", "B"])
                    >>> c2 < c0
                    True

                It is possible for neither of two conversions to be
                preferable:

                    >>> c1 < c2
                    False
                    >>> c2 < c1
                    False
                """
                cmp = len(self.seq) - len(other.seq)
                if cmp:
                        return cmp < 0
                return (self.ask < other.ask and other.bid <= self.bid or
                        self.ask <= other.ask and other.bid < self.bid)

        def __add__(self, other):
                """
                Return a Conversion representing the result of performing
                the other Conversion after this one.

                    >>> c0 = Conversion(1, 2, ["A"])
                    >>> c1 = Conversion(3, 4, ["B"])
                    >>> c0 + c1
                    Conversion(3, 8, ['A', 'B'])
                """
                return Conversion(
                        self.ask * other.ask,
                        self.bid * other.bid,
                        self.seq + other.seq
                )

        def __iter__(self):
                """An iterator over this Conversion's tradeable pairs."""
                return iter(self.seq)

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
