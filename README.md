Balanced Auto Trader
====================

This program implements an obsessive-compulsive trader that keeps a
portfolio in balance. The desired proportion of each asset can be specified
in a configuration file. Trades will be generated whenever any asset's
value rises or falls outside of its target range by some specified margin
of error.

The program is designed to be run on a regular basis, performing a number
of trades on each run. The run ends when orders have been submitted, but
not necessarily executed by the exchange.

It is not assumed that all assets are directly tradeable for one another,
so it may take several runs for a portfolio to be completely re-balanced.
The code is designed to make steps towards reaching this goal every time it
is run.

Assets are valued strictly relative to one another without relying on any
reference currency. For this purpose, exchange rates of tradeable pairs are
transitively extended and each asset is valuated by the norm in terms of
all assets in the portfolio.


Command line
------------

```
usage: bat [-h] [-n] [-v] [file [file ...]]

Keep a portfolio in balance.

positional arguments:
  file        configuration file

optional arguments:
  -h, --help  show this help message and exit
  -n          do a dry run
  -v          produce verbose output
```

Any number of configuration files may be specified on the command line.
Standard input is read if none are provided.

The command line option `-n` limits the action of the program to printing the
trades that would be submitted based on the current balance.

The command line option `-v` causes the output to be more verbose.


Configuration file
------------------

The configuration file contains the class name of the trading backend
implementation and any data required to create an instance, such as the API
host and account credentials of the trading platform. Furthermore, it specifies
the error margin and the desired target portfolio. The following example shows
a portfolio consisting of some proportions of Bitcoin, Ethereum and Z-Cash
crypto-currencies.

```json
{
 "account": {
  "class": "bat.kraken.Account",
  "apiHost": "https://api.kraken.com",
  "publicKey": "...",
  "secretKey": "..."
 },
 "error": 0.03,
 "portfolio": {
  "XXBT": 3,
  "XETH": 2,
  "XZEC": 1
 }
}
```


Trading backend
---------------

Currently, the only implemented backends are for the [Bittrex][BTX],
[HitBTC][HIT] and [Kraken][KRK] crypto-currency exchanges. The code has
been designed to allow adding other trading backends and [API
documentation][API] is available, but slight adjustments may need to be
made to the configuration file format and some of the internal data
structures.


Testing
-------

The included unit tests can be run using

    python3 -m unittest discover src '*.py'

After adding the src directory to the python path, tests for a particular
module may be run with

    python3 -m unittest bat.trade

Run the linter using

    pylint src/bat


[API]:  https://jkuebart.gitlab.io/Bat/bat/index.html
[BTX]:  https://bittrex.com/
[HIT]:  https://hitbtc.com/
[KRK]:  https://www.kraken.com/
