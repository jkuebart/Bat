# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Polyfill for Python 3.5's isclose.
"""

__all__ = ["isclose"]

from math import isinf

def isclose_impl(lhs, rhs, rel_tol=1e-9, abs_tol=0):
        """
        Determines whether two floating point numbers are close in
        value.

            >>> isclose_impl(123, 124, rel_tol=0.001)
            False
            >>> isclose_impl(123, 124, rel_tol=0.01)
            True
            >>> isclose_impl(0, 0, rel_tol=0)
            True
            >>> isclose_impl(0, float("inf"))
            False
            >>> isclose_impl(0, 0, rel_tol=-1)
            Traceback (most recent call last):
                ...
            ValueError: tolerances must be non-negative
        """
        if rel_tol < 0 or abs_tol < 0:
                raise ValueError("tolerances must be non-negative")

        if lhs == rhs:
                return True

        if isinf(lhs) or isinf(rhs):
                return False

        return abs(lhs - rhs) <= max(abs_tol, rel_tol * max(abs(lhs), abs(rhs)))

try:
        from math import isclose
except ImportError: # Python < 3.5
        isclose = isclose_impl

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
