# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""Normalisation provides a scale-invariant valuation for a set of assets."""

__all__ = ["Normalisation"]

import logging
from math import sqrt

LOGGER = logging.getLogger(__name__)

def norm(vector):
        """
        The Euclidean distance.

            >>> norm([3, 4])
            5.0
        """
        return sqrt(sum(x * x for x in vector))

class Normalisation:
        """I provide a scale-invariant valuation for a set of assets."""

        def __init__(self, ticker):
                """
                Create a new Normalisation given a Ticker.

                In order to eliminate bias caused by assets of widely
                varying value, valuations in terms of an asset are always
                normalised using the norm of the quote asset's value in
                terms of all other assets.
                """
                self.ticker = ticker

                assets = ticker.get_assets()
                self._normalisation = {
                        base: norm(ticker.bid(base, quote) for quote in assets)
                        for base in assets
                }
                LOGGER.debug("normalisation %r", self._normalisation)

        def get_ticker(self):
                """Return the Ticker instance."""
                return self.ticker

        def valuate(self, base):
                """
                Return the asset's normalised value.

                Let A and B be assets with the following valuation.

                    >>> from unittest.mock import Mock

                    >>> valuation = {
                    ...     "A": {"A": 1, "B": 2},
                    ...     "B": {"A": 0.5, "B": 1},
                    ... }

                    >>> ticker = Mock()
                    >>> ticker.get_assets = valuation.keys
                    >>> ticker.bid = lambda base, quote: valuation[base][quote]

                Then the normalised valuation works out as:

                    >>> n = Normalisation(ticker)
                    >>> round(n.valuate("A"), 3)
                    1.844
                    >>> round(n.valuate("B"), 3)
                    0.922

                It is apparent that according to the normalisation, A is
                worth about twice as much as B, a fact which is confirmed
                by the valuation table.
                """
                assets = self.ticker.get_assets()
                return norm(self.ticker.bid(base, quote) /
                            self._normalisation[quote]
                            for quote in assets)

def load_tests(ignored_loader, tests, ignored_pattern):
        import doctest
        tests.addTests(doctest.DocTestSuite())
        return tests
