# Copyright 2018-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

image: 'python:slim'

# Definitions for jobs using pip.
.pip-cache: &pip-cache
    variables:
        # Pip's cache can only be cached if it's in the project directory.
        PIP_CACHE_DIR: '${CI_PROJECT_DIR}/.cache'

    # Cache python dependencies for next run of the same job.
    cache:
        key: '${CI_JOB_NAME}'
        paths:
          - '${PIP_CACHE_DIR}'

#
# Unit tests.
#
test:
    <<: *pip-cache

    before_script:
      - 'pip3 install coverage'

    # Generate HTML and console output using coverage.py.
    script: |
        coverage run --branch -m unittest discover src '*.py'
        coverage html
        coverage report

    # Parse coverage.py's console output.
    coverage: '/^TOTAL.*\s(\d+%)$/'

    # Pass HTML report on for publishing.
    artifacts:
        paths:
          - 'htmlcov'

#
# Unit tests on Python3.4.
#
test34:
    # Debian jessie has Python3.4.
    image: 'python:3.4-slim-jessie'

    script: |
        python3 -m unittest discover src '*.py'

#
# Linter.
#
lint:
    <<: *pip-cache

    before_script:
      - 'pip3 install pylint --pre'

    # Ignore pylint's exit status.
    script: |
        pylint src/bat || true

#
# API documentation.
#
documentation:
    <<: *pip-cache

    before_script:
      - 'pip3 install pdoc3'

    script: |
        PYTHONPATH=src pdoc --html bat

    # Pass documentation on for publishing.
    artifacts:
        paths:
          - 'html/bat'

#
# Publishing.
#
pages:
    stage: 'deploy'

    # Only publish pages built on the master branch.
    only:
      - 'main'

    variables:
        # We don't need a git workspace.
        GIT_STRATEGY: 'none'

    script: |
        mv html public
        mv htmlcov public

    artifacts:
        paths:
          - 'public'
