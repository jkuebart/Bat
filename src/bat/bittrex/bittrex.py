# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Bittrex represents a connection to the bittrex endpoint and provides
methods for accessing its public API methods.
"""

__all__ = ["Bittrex", "load"]

from urllib.parse import urlencode
from urllib.request import Request, urlopen

from bat import json
from bat.decimals import decimals
from bat.ticker import Ticker

VERSION = "v1.1"
URL_PATH = "/%s/%s/%s"

def make_url(method, realm):
        """Return the URL path for the given method and realm."""
        return URL_PATH % (VERSION, realm, method)

def get_result(response):
        """Return the result of the response or raise an error."""
        if not response["success"]:
                raise RuntimeError(response["message"])
        return response["result"]

def load(url, headers={}):
        """Return the result of the specified query."""
        with urlopen(Request(url, headers=headers)) as resp:
                return get_result(json.load(resp))

class Bittrex:
        """Interact with Bittrex's public API."""

        def __init__(self, api_host):
                self.api_host = api_host

        def url(self, method, realm, data=None):
                """Build an API URL from the given URL path."""
                return "%s%s%s" % (self.api_host,
                                   make_url(method, realm),
                                   "?%s" % (urlencode(data),) if data else "")

        def request(self, method, data=None, headers={}):
                """Send a public request to the API."""
                return load(self.url(method, "public", data), headers)

        def asset_pairs(self):
                """Invoke the getmarkets API method."""
                return {
                        pair["MarketName"]: {
                                "base": pair["MarketCurrency"],
                                "price_decimals": -9,
                                "quote": pair["BaseCurrency"],
                                "volume_decimals": decimals(pair["MinTradeSize"]),
                        }
                        for pair in self.request("getmarkets")
                        if pair["IsActive"]
                }

        def ticker(self, asset_pairs):
                """Invoke the Ticker API method."""
                return Ticker(
                        (pair_name,
                         asset_pairs[pair_name]["base"],
                         asset_pairs[pair_name]["quote"],
                         data["Ask"],
                         data["Bid"])
                        for pair_name, data in (
                                (pair, self.request("getticker", {
                                        "market": pair
                                }))
                                for pair in asset_pairs.keys()
                        )
                )
