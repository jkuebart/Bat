# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Account represents a connection to some kraken account and provides
methods for accessing its private API methods.
"""

__all__ = ["Account"]

from base64 import b64decode, b64encode
from hashlib import sha256, sha512
import hmac
import logging
from urllib.parse import urlencode

from bat.noncegenerator import NonceGenerator
from bat.trade import Trade

from .kraken import Kraken, make_url

LOGGER = logging.getLogger(__name__)

class Account:
        """Access a private Kraken account."""

        def __init__(self, config):
                self._kraken = Kraken(config["apiHost"])
                self._public_key = config["publicKey"]
                self._secret_key = b64decode(config["secretKey"])

                self._nonce = NonceGenerator()

        def xchg(self):
                """The exchange used by this account."""
                return self._kraken

        def request(self, method, data={}):
                """Send a signed request to this account."""
                url_path = make_url(method, "private")
                nonce = self._nonce.get_nonce()

                data = dict(data)
                data["nonce"] = nonce
                data = urlencode(data)

                sign = url_path.encode("utf-8")
                sign += sha256((nonce + data).encode("utf-8")).digest()
                sign = hmac.new(self._secret_key, sign, sha512)

                return self._kraken.load(url_path, data, {
                        "API-Key": self._public_key,
                        "API-Sign": b64encode(sign.digest()),
                })

        def balance(self):
                """Invoke the Balance API method."""
                return {
                        base: float(val)
                        for base, val in self.request("Balance").items()
                        if float(val)
                }

        def open_orders(self):
                """Invoke the OpenOrders API method."""
                orders = self.request("OpenOrders")["open"]
                if not orders:
                        return {}

                # Kraken returns open orders with the "altname"s, so
                # convert them back.
                pair_names = {
                        pair["altname"]: pair_name
                        for pair_name, pair in self._kraken.asset_pairs(
                                [ordr["descr"]["pair"] for ordr in orders.values()]
                        ).items()
                }
                LOGGER.debug("open_orders pair_names %r", pair_names)

                return {
                        txid: Trade(
                                ordr["descr"]["type"],
                                pair_names[ordr["descr"]["pair"]],
                                float(ordr["vol"]),
                                float(ordr["descr"]["price"]),
                        )
                        for txid, ordr in orders.items()
                }

        def cancel_order(self, txid):
                """Invoke the CancelOrder API method."""
                res = self.request("CancelOrder", {"txid": txid})
                if not res["count"]:
                        raise RuntimeError("cancel_order %r" % (res,))
                return self

        def add_trade(self, trade, dry_run=False):
                """Invoke the AddOrder API method."""
                asset_pair = trade.asset_pair
                if ("unit" != asset_pair["lot"] or
                    1 != asset_pair["lot_multiplier"]):
                        raise RuntimeError(
                                "Do not know how to trade pair %r" % asset_pair
                        )

                order = {
                        "ordertype": "limit",
                        "pair": trade.pair,
                        "price": trade.format_price(),
                        "type": trade.type,
                        "volume": trade.format_volume(),
                }
                if dry_run:
                        order["validate"] = True

                res = self.request("AddOrder", order)
                if not dry_run and not res.get("txid", []):
                        raise RuntimeError("add_trade %r" % res)
                return res
