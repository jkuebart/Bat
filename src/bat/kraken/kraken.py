# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Kraken represents a connection to the kraken endpoint and provides methods
for accessing its public API methods.
"""

__all__ = ["Kraken", "make_url"]

import logging
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from bat import json
from bat.ticker import Ticker

VERSION = 0
URL_PATH = "/%s/%s/%s"

LOGGER = logging.getLogger(__name__)

def make_url(method, realm):
        """Return the URL path for the given method and realm."""
        return URL_PATH % (VERSION, realm, method)

def get_result(response):
        """Return the result of the response, raise on error."""
        if response["error"]:
                for err in response["error"][1:]:
                        LOGGER.error("%s", err)
                raise RuntimeError(response["error"][0])
        return response["result"]

class Kraken:
        """Interact with Kraken's public API."""

        def __init__(self, api_host):
                self.api_host = api_host

        def url(self, url_path):
                """Build an API URL from the given URL path."""
                return "%s%s" % (self.api_host, url_path)

        def load(self, url_path, data=None, headers={}):
                """Return the result of the specified query."""
                url = self.url(url_path)
                data = data and data.encode("ascii")
                with urlopen(Request(url, data, headers)) as resp:
                        return get_result(json.load(resp))

        def request(self, method, data=None, headers={}):
                """Send a public request to the API."""
                url_path = make_url(method, "public")
                data = data and urlencode(data)
                return self.load(url_path, data, headers)

        def time(self):
                """Invoke the Time method."""
                return self.request("Time")

        def assets(self, assets):
                """Invoke the Assets method."""
                return self.request("Assets", {
                        "info": "info",
                        "aclass": "currency",
                        "asset": ",".join(assets),
                })

        def asset_pairs(self, pairs=None):
                """Invoke the AssetPairs method."""
                data = {"info": "info"}
                if pairs:
                        data["pair"] = ",".join(pairs)
                return {
                        name: {
                                "base": pair["base"],
                                "price_decimals": -int(pair["pair_decimals"]),
                                "quote": pair["quote"],
                                "volume_decimals": -int(pair["lot_decimals"]),

                                # Used internally:
                                "altname": pair["altname"],
                                "lot": pair["lot"],
                                "lot_multiplier": pair["lot_multiplier"],
                        }
                        for name, pair in self.request("AssetPairs", data).items()
                }

        def ticker(self, asset_pairs):
                """Invoke the Ticker API method."""
                return Ticker(
                        (pair_name,
                         asset_pairs[pair_name]["base"],
                         asset_pairs[pair_name]["quote"],
                         float(data["a"][0]),
                         float(data["b"][0]))
                        for pair_name, data in self.request("Ticker", {
                                "pair": ",".join(asset_pairs.keys())
                        }).items()
                        if float(data["a"][0]) and float(data["b"][0])
                )
