# Copyright 2017-2020, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Account represents a connection to some hitbtc account and provides
methods for accessing its private API methods.
"""

__all__ = ["Account"]

from base64 import b64encode

from bat.trade import Trade

from .hitbtc import HitBtc

class Account:
        def __init__(self, config):
                self._hitbtc = HitBtc(config["apiHost"])

                auth = "%s:%s" % (config["publicKey"], config["secretKey"])
                self._auth = b64encode(auth.encode("ascii")).decode("ascii")

        def xchg(self):
                return self._hitbtc

        def request(self, action, realm=None, data=None, method=None):
                """Send a signed request to this account."""
                headers = {"authorization": "basic %s" % (self._auth,)}
                return self._hitbtc.load(action, realm, data, headers, method)

        def balance(self):
                """Invoke the balance API method."""
                return {
                        bal["currency"]: float(bal["available"])
                        for bal in self.request("balance", "trading")
                        if "0" != bal["available"]
                }

        def open_orders(self):
                """Invoke the order API method."""
                return {
                        ordr["clientOrderId"]: Trade(
                                ordr["side"],
                                ordr["symbol"],
                                float(ordr["quantity"]),
                                float(ordr["price"])
                        )
                        for ordr in self.request("order")
                }

        def cancel_order(self, txid):
                """Invoke the cancel API method."""
                return self.request(txid, "order", method="DELETE")

        def add_trade(self, trade, dry_run=False):
                """Invoke the buylimit or selllimit API method."""
                order = {
                        "price": trade.format_price(),
                        "quantity": trade.format_volume(),
                        "side": trade.type,
                        "strictValidate": True,
                        "symbol": trade.pair,
                        "timeInForce": "GTC",
                        "type": "limit"
                }
                if dry_run:
                        return order
                return self.request("order", data=order)
